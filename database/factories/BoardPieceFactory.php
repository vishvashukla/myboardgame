<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\BoardPiece::class, function (Faker\Generator $faker) {
//    $commands=["left","right","up","down"];
//    shuffle($commands);
//    $newCommands = implode(',', $commands);
//    return [
//          'commands'=>$newCommands,
//    ];
    return [
        'commands'=>implode(',',$faker->randomElements(['left','right','up','down'],mt_rand(1,4))),
    ];
});
