<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Game;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user) {
            $randomNo=mt_rand(1,3);
            factory(App\Game::class, $randomNo)->create(['user_id'=>$user->id]);
        }
    }
}
