<?php

use Illuminate\Database\Seeder;
use App\Board;
use App\Piece;
use App\BoardPiece;

class BoardPiecesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boards=Board::all();
        foreach ($boards as $board)
        {
            $randomPiece=rand(1,3);
            $boardPieceObj=new BoardPiece;
            $pieceObj=new Piece;
            for($count=0;$count<$randomPiece;$count++)
            {
                $flag=true;
                $piece['x']=mt_rand(1,$board->rows);
                $piece['y']=mt_rand(1,$board->columns);
                if($boardPieceObj->isPieceExists($piece,$board->id))
                {
                    $flag=false;
                }
                if($flag)
                {
                    if(!$pieceObj->isPieceExists($piece))
                    {
                        factory(App\Piece::class)->create(['x'=>$piece['x'],'y'=>$piece['y']]);
                    }
                    $getPiece=$pieceObj->getPiece($piece);
                    factory(App\BoardPiece::class)->create(['board_id'=>$board->id,'piece_id'=>$getPiece['id'],'x'=>$getPiece['x'],'y'=>$getPiece['y']]);
                }
                else
                {
                    $count=$count-1;
                }
            }
        }
    }
}
