<?php

use Illuminate\Database\Seeder;
use App\Game;


class BoardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games=Game::all();
        foreach ($games as $game)
        {
            factory(App\Board::class,1)->create(['game_id'=>$game->id]);
        }
    }
}
