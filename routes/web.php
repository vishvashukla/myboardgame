<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

//Route::any('/newgame','BoardPieceGameController@newGame')->name('newgame')->middleware('auth','verified');
//
//Route::any('/displaygame','BoardPieceGameController@startGame')->name('displaygame')->middleware('auth','verified');
Route::get('/newgame',function ()
{
    return view('getBoardData');
})->name('newgame')->middleware('auth','verified');

Route::post('/newgame','BoardPieceGameController@newGame')->name('newgame')->middleware('auth','verified');

Route::get('/displaygame','BoardPieceGameController@startGame')->name('displaygame')->middleware('auth','verified');
Route::post('/displaygame','BoardPieceGameController@startGame')->name('displaygame')->middleware('auth','verified');

Route::get('verifyemail',function ()
{
   return view('verifyemail');
});

Route::get('admin',function ()
{
    return view('adminprofile');

})->name('admin')->middleware('auth','checkadmin');

Route::get('user',function ()
{
//    $user=Auth::user();
//    if(!$user->is_admin)
//    {
//        return view('userpage');
//    }
//    else
//    {
//        return redirect('admin');
//    }
    return view('userpage');
})->name('user')->middleware('auth','checkuser');

Route::get('email',function ()
{
    return view('emails.email');

});

Route::get('data',function ()
{
    return view('getBoardData');

});

Route::get('test', 'BoardController@newTest');
Route::get('testboard', function ()
{
    return view('testGetBoardData');
});
//Route::get('test',function ()
//{
//    return view('test');
//
//});

//Route::get('/board','BoardGameController@boardPieceGame')->name('/board')->middleware('auth','verified');
//Route::get('boardgame','BoardGameDbController@boardGame')->name('boardgame')->middleware('auth','verified');
//Route::get('mytest','BoardGameDbController@testing');

