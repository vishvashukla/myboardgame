<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Move extends Model
{
    protected $table="moves";
    protected $fillable=["id","board_id","piece_id","command","is_active"];

    public function board()
    {
        return $this->belongsTo('App\Board');
    }
    public function piece()
    {
        return $this->belongsTo('App\Piece');
    }
    public function createMove($direction,$board_id,$boardPiece_id)
    {
        Move::create(['command' => $direction, 'board_id' => $board_id, 'piece_id' => $boardPiece_id, 'is_active' => true]);
    }
    public function updateMove($moveId)
    {
        Move::where('id',$moveId)->update(['is_active'=>false]);
    }
}
