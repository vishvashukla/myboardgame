<?php
/**
 * Created by PhpStorm.
 * User: levaral
 * Date: 01/02/18
 * Time: 7:10 PM
 */

namespace App\Games;

class BoardPieceGame
{
    public $piece = [];
    private $board = [];

    public function __construct($noOfRows,$noOfColumns)
    {
        $this->board['rows']=$noOfRows;
        $this->board['columns']=$noOfColumns;

    }
    public function createPiece($xPosition, $yPosition)
    {
        $this->piece['x'] = $xPosition;
        $this->piece['y'] = $yPosition;
    }
    public function getBoard()
    {
        return $this->board;
    }
    public function getPiece()
    {
        return $this->piece;
    }
    public function setPiece($piece)
    {
        return $this->piece = $piece;

    }
    public function up($piece)
    {
        if ($piece['x'] + 1 > $this->board['rows']) {
            session()->put('flag',false);
            return $piece;
        }
        $piece['x'] = $piece['x'] + 1;
        return $piece;
    }

    public function down($piece)
    {
        if ($piece['x'] - 1 < 1) {
            session()->put('flag',false);
            return $piece;
        }
        $piece['x'] = $piece['x'] - 1;
        return $piece;
    }

    public function left($piece)
    {
        if ($piece['y'] - 1 < 1) {
            session()->put('flag',false);
            return $piece;
        }
        $piece['y'] = $piece['y'] - 1;
        return $piece;
    }

    public function right($piece)
    {
        if ($piece['y'] + 1 >$this->board['columns']) {
            session()->put('flag',false);
            return $piece;
        }
        $piece['y'] = $piece['y'] + 1;
        return $piece;
    }
    function move($direction,$piece)
    {

        if ($direction == 'up') {
            $piece = $this->up($piece);

        } else if ($direction == 'down') {
            $piece = $this->down($piece);

        } else if ($direction == 'left') {
            $piece = $this->left($piece);

        } else if ($direction == 'right') {
            $piece = $this->right($piece);

        }
        else {
            session()->put('flag',false);
            return $piece;
        }
        return $piece;
    }

}