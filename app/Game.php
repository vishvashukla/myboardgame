<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table="games";
    protected $fillable=["id","status","user_id"];

    public function board()
    {
        return $this->hasOne('App\Board');
    }
    public function createGame($user_id)
    {
        Game::create(['status' => true,'user_id'=>$user_id]);
    }
    public function updateGame()
    {
        Game::where('status',true)->update(['status'=>false]);
//        foreach ($games as $game) {
//            $game->status = false;
//            $game->save();
//        }
    }
    public function getGameId($user_id)
    {
        $gameRun = Game::where('user_id',$user_id)->where('status', true)->first();
        return $gameRun['id'];
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
