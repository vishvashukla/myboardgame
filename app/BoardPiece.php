<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardPiece extends Model
{
    protected $table="board_pieces";
    protected $fillable=["id","board_id","piece_id","x","y","commands","is_active"];
    public function board()
    {
        return $this->belongsTo('App\Board');
    }
    public function piece()
    {
        return $this->belongsTo('App\Piece');
    }
    public function isBoardPieceExists($piece,$board_id,$piece_id)
    {
        $piecesData=BoardPiece::all()->where('board_id',$board_id)->whereNotIn('piece_id',$piece_id);
        foreach ($piecesData as $pieceData)
        {
            if($pieceData['x']==$piece['x'] && $pieceData['y']==$piece['y'])
            {
                session()->put('flag',false);
                return true;
            }

        }
        return false;
    }
    public function createBoardPiece($board_id,$pieceNew,$commands)
    {
        BoardPiece::create(['board_id' => $board_id, 'piece_id' => $pieceNew['id'], 'x' => $pieceNew['x'], 'y' => $pieceNew['y'], 'commands' => $commands]);
    }
//    public function inActiveBoardPiece()
//    {
//        $boardPieces = BoardPiece::where('is_active',true)->update(['is_active'=>false]);
////        foreach ($boardPieces as $boardPiece) {
////            $boardPiece->is_active = false;
////            $boardPiece->save();
////        }
//    }
    public function updateBoardPiece($piece_id,$board_id,$newDirections)
    {
        BoardPiece::where('piece_id', $piece_id)
            ->where('board_id',$board_id)
            ->update(['commands' => $newDirections]);
    }
//    public function getBoardPieces()
//    {
//        $piece=[];
//        $boardPieceDatas = BoardPiece::where('is_active', true)->get();
//        foreach ($boardPieceDatas as $boardPieceData) {
//            $piece[] = ['x' => $boardPieceData['x'], 'y' => $boardPieceData['y']];
//        }
//    }
    public function updatePiecePosition($piece_id,$board_id,$newPiece)
    {
        BoardPiece::where('piece_id',$piece_id)
            ->where('board_id',$board_id)
            ->update(['x'=>$newPiece['x'],'y'=>$newPiece['y']]);
    }
    public function isPieceExists($piece,$board_Id)
    {
        $boardPieces=BoardPiece::where('board_id',$board_Id)->get();
        foreach ($boardPieces as $boardPiece)
        {
            if($boardPiece['x']==$piece['x'] && $boardPiece['y']==$piece['y'])
            {
                return true;
            }
        }
        return false;
    }
}
