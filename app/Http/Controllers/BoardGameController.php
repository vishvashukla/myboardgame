<?php
/**
 * Created by PhpStorm.
 * User: levaral
 * Date: 09/02/18
 * Time: 10:06 AM
 */

namespace App\Http\Controllers;
use App\Game;
use App\Move;
use Illuminate\Http\Request;
use App\Board;
use App\BoardPiece;
use App\Piece;
use App\Games\BoardPieceGame;
use Illuminate\Support\Facades\Auth;

class BoardGameController extends Controller
{
    public function boardPieceGame(Request $request)
    {
        $user=Auth::user();
        $piecesObj=new Piece;
        $boardsObj=new Board;
        $gamesObj=new Game;
        $boardPiecesObj=new BoardPiece;
        $movesObj=new Move;
        $piece=[];
        if($request->has('newGame'))
        {
            $rows = $request->input('rows');
            $columns = $request->input('columns');
            $counter = $request->input('counter');
            $board['rows']=$rows;
            $board['columns']=$columns;
          //  $gamesObj->updateGame();
          //  $boardPiecesObj->inActiveBoardPiece();
            $gamesObj->createGame($user->id);
            $gameId=$gamesObj->getGameId();
//            if($boardsObj->isBoardExists($board))
//            {
//                $boardsObj->updateBoard($board,$gameId);
//            }
//            else
//            {
                $boardsObj->createBoard($board,$gameId);
//            }
            $gameRun = Game::where('status', true)->first();
            $board=$gameRun->board()->first();
            for ($count = 1; $count <= $counter; $count++) {
                $piece['x'] = $request->input('x' . $count);
                $piece['y'] = $request->input('y' . $count);
                $commands = $request->input('commands' . $count);
                if(!$boardPiecesObj->isPieceExists($piece,$board['id']))
                {
                    if(!$piecesObj->isPieceExists($piece))
                    {
                          $piecesObj->createPiece($piece);
                    }
                     $pieceNew = $piecesObj->getPieceId($piece);

                        $boardPiecesObj->createBoardPiece($board['id'],$pieceNew,$commands);
                }
//                if(!$piecesObj->isPieceExists($piece))
//                {
//                    $piecesObj->createPiece($piece);
//                }
//                $pieceNew = $piecesObj->getPieceId($piece);
//                if(!$boardPiecesObj->isPieceExists($pieceNew,$board['id']))
//                {
//                    $boardPiecesObj->createBoardPiece($board['id'],$pieceNew,$commands);
//                }
            }
            $piece = [];
            $boardPieces = BoardPiece::where('is_active', true)->where('board_id',$board['id'])->get();
            foreach ($boardPieces as $boardPieceData) {
                $piece[] = ['x' => $boardPieceData['x'], 'y' => $boardPieceData['y']];
            }
            session()->put('flag',true);
            session()->put('gameOver',false);
            return view('printPiece')->with(['board' => $board, 'piece' => $piece]);
        }

        if($request->has('startGame'))
        {
            $gameRun = Game::where('status', true)->first();
            $board=$gameRun->board()->first();
            session()->put('gameOver',false);
         //   $game= Game::where('status', true)->first();
            $game= Game::where('status', true)->first();
            if(empty($game))
            {
                return redirect('/board');
            }
            $board=$game->board()->first();
            $boardPieceGameObj=new BoardPieceGame($board['rows'],$board['columns']);
            session()->put('flag',true);

            $boardPieces=$board->board_piece()->get();
            foreach ($boardPieces as $boardPiece) {
                $directions = explode(",", $boardPiece['commands']);
                if (!empty($directions)) {
                    $direction = array_shift($directions);
                    if ($direction != null) {
                        $newDirections = implode(',', $directions);
                        $movesObj->createMove($direction,$board['id'],$boardPiece['piece_id']);
                        $boardPiecesObj->updateBoardPiece($boardPiece['piece_id'],$boardPiece['board_id'],$newDirections);
                    }
                } else {
                    break;
                }
            }
            $moveDatas = Move::where('board_id', $board['id'])->where('is_active', true)->first();
            $moveId = $moveDatas['id'];
            if ($moveDatas != null) {
                $getPieceData = BoardPiece::where('is_active', true)
                    ->where('board_id',$moveDatas['board_id'])
                    ->where('piece_id', $moveDatas['piece_id'])->first();
                $boardPieceGameObj->createPiece($getPieceData['x'], $getPieceData['y']);
                $newPiece = $boardPieceGameObj->move($moveDatas['command'], $boardPieceGameObj->piece);

                if (session()->get('flag') && !$boardPiecesObj->isBoardPieceExists($newPiece,$moveDatas['board_id'], $moveDatas['pieceId'])) {
                    $boardPiecesObj->updatePiecePosition($moveDatas['piece_id'],$moveDatas['board_id'], $newPiece);
                } else {
                    Move::where('piece_id', $moveDatas['piece_id'])
                        ->where('board_id',$moveDatas['board_id'])
                        ->where('is_active', true)
                        ->update(["command" => "", "is_active" => false]);
                    BoardPiece::where('piece_id', $moveDatas['piece_id'])
                        ->where('board_id',$moveDatas['board_id'])
                        ->where('is_active', true)->update(["commands" => ""]);
                }
                $movesObj->updateMove($moveId);
                session()->put('gameOver', false);
            } else {
                session()->put('gameOver', true);
            }
//            $moveDatas=Move::where('board_id',$board['id'])->where('is_active',true)->first();
//            $moveId=$moveDatas['id'];
//            if($moveDatas!=null)
//            {
//                $getPieceData=BoardPiece::where('is_active',true)->where('piece_id',$moveDatas['piece_id'])->first();
//                $boardPieceGameObj->createPiece($getPieceData['x'], $getPieceData['y']);
//                $newPiece=$boardPieceGameObj->move($moveDatas['command'],$boardPieceGameObj->piece);
//                if(session()->get('flag') && !$boardPiecesObj->isBoardPieceExists($newPiece,$moveDatas['pieceId']))
//                {
//                    $boardPiecesObj->updatePiecePosition($moveDatas['piece_id'],$newPiece);
//                }
//                else
//                {
//
//                    Move::where('piece_id',$moveDatas['piece_id'])->where('is_active',true)->update(["command"=>"","is_active"=>false]);
//                    BoardPiece::where('piece_id',$moveDatas['piece_id'])->where('is_active',true)->update(["commands"=>""]);
//
////                        foreach ($moves as $move)
////                        {
////                            $move->command="";
////                            $move->is_active=false;
////                            $move->save();
////                        }
////                        Move::where('piece_id', $moveDatas['piece_id'])
////                            ->where('is_active', true)
////                            ->update(['command' => "",'is_active'=>false]);
//                    // Move::where('piece_id',$moveDatas['piece_id'])->where('is_active',true)->update(['command'=>"",'is_active'=>false]);
//                }
//                $movesObj->updateMove($moveId);
//                session()->put('gameOver',false);
//            }
//            else
//            {
//                session()->put('gameOver',true);
//                Game::where('id',$game->id)->update(['status'=>false]);
//
//            }
            if(!session()->get('gameOver'))
            {
                ?>
                <script>
                    setTimeout(function () {
                        location.reload()
                    },2000);
                </script>

                <?php
            }
            $piece=[];
           // $boardPieceDatas = BoardPiece::where('is_active', true)->get();
           // $boardPieces = BoardPiece::where('is_active', true)->where('board_id',$board['id'])->get();
            $boardPieces=$board->board_piece()->get();
            foreach ($boardPieces as $boardPiece) {
                $piece[] = ['x' => $boardPiece['x'], 'y' => $boardPiece['y']];
            }
            if(session()->get('gameOver'))
            {
                Game::where('id', $game->id)->update(['status' => false]);
                BoardPiece::where('is_active', true)->where('board_id', $board['id'])->update(['is_active' => false]);
               // BoardPiece::where('is_active', true)->where('board_id',$board['id'])->update(['is_active'=>false]);
            }
            return view('printPiece')->with(['board' => $board, 'piece' => $piece]);
        }
        return view('getBoardData');
    }


}