<?php
/**
 * Created by PhpStorm.
 * User: levaral
 * Date: 19/02/18
 * Time: 11:55 AM
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use App\Game;
use App\Move;
use App\Board;
use App\BoardPiece;
use App\Piece;
use App\Games\BoardPieceGame;
use Illuminate\Support\Facades\Auth;

class BoardGameDbController extends Controller
{
    public function boardGame(Request $request)
    {
        $boardPiecesObj = new BoardPiece;
        $movesObj = new Move;
        $user = Auth::user();
        $game = Game::where('user_id', $user->id)->where('status', true)->first();
        if (empty($game)) {
            return redirect('/board');
        }
        $board = $game->board()->first();
        if ($request->has('nextGame')) {
            Game::where('id', $game->id)->update(['status' => false]);
            BoardPiece::where('board_id', $board['id'])
           // ->where('is_active', true)
                ->update(['is_active' => false]);
            $game = Game::where('user_id', $user->id)->where('status', true)->first();
            if (empty($game)) {
                return redirect('/board');
            }
        }
        if ($request->has('startGame')) {
            $game = Game::where('user_id', $user->id)->where('status', true)->first();
            $board = $game->board()->first();
            session()->put('gameOver', false);
            $boardPieceGameObj = new BoardPieceGame($board['rows'], $board['columns']);
            session()->put('flag', true);
            $boardPieces=$board->board_piece()->get();
            foreach ($boardPieces as $boardPiece) {
                $directions = explode(",", $boardPiece['commands']);
                if (!empty($directions)) {
                    $direction = array_shift($directions);
                    if ($direction != null) {
                        $newDirections = implode(',', $directions);
                        $movesObj->createMove($direction, $board['id'], $boardPiece['piece_id']);
                        $boardPiecesObj->updateBoardPiece($boardPiece['piece_id'],$boardPiece['board_id'],$newDirections);
                    }
                } else {
                    break;
                }
            }
            echo "Game ID".$game['id'];
            echo "board Id".$board['id'];
            $moveDatas = Move::where('board_id', $board['id'])->where('is_active', true)->first();
            $moveId = $moveDatas['id'];
            if ($moveDatas != null) {
                $getPieceData = BoardPiece::where('board_id',$moveDatas['board_id'])
                    //->where('is_active', true)
                    ->where('piece_id', $moveDatas['piece_id'])->first();
                $boardPieceGameObj->createPiece($getPieceData['x'], $getPieceData['y']);
                $newPiece = $boardPieceGameObj->move($moveDatas['command'], $boardPieceGameObj->piece);

                if (session()->get('flag') && !$boardPiecesObj->isBoardPieceExists($newPiece,$moveDatas['board_id'], $moveDatas['pieceId'])) {
                    $boardPiecesObj->updatePiecePosition($moveDatas['piece_id'],$moveDatas['board_id'], $newPiece);
                } else {
                    Move::where('piece_id', $moveDatas['piece_id'])
                        ->where('board_id',$moveDatas['board_id'])
                        ->where('is_active', true)
                        ->update(["command" => "", "is_active" => false]);
                    BoardPiece::where('piece_id', $moveDatas['piece_id'])
                        ->where('board_id',$moveDatas['board_id'])
                     //   ->where('is_active', true)
                        ->update(["commands" => ""]);
                }
                $movesObj->updateMove($moveId);
                session()->put('gameOver', false);
            } else {
                session()->put('gameOver', true);
            }
            if (!session()->get('gameOver')) {
                ?>
                <script>
                    setTimeout(function () {
                        location.reload()
                    }, 2000);
                </script>
                <?php
            }
            $piece = [];
            $boardPieces=$board->board_piece()->get();
            foreach ($boardPieces as $boardPiece) {
                $piece[] = ['x' => $boardPiece['x'], 'y' => $boardPiece['y']];
            }
//            if (session()->get('gameOver')) {
//                //  BoardPiece::where('is_active', true)->where('board_id',$board['id'])->update(['is_active'=>false]);
//            }
            return view('printPiece')->with(['board' => $board, 'piece' => $piece]);
        }
        $game = Game::where('user_id', $user->id)->where('status', true)->first();
        $board = $game->board()->first();
        $piece = [];
        $commands = [];
        $boardPieces=$board->board_piece()->get();
        foreach ($boardPieces as $boardPiece) {
            $piece[] = ['x' => $boardPiece['x'], 'y' => $boardPiece['y']];
            $commands[] = $boardPiece['commands'];
        }
        session()->put('flag', true);
        session()->put('gameOver', false);

        return view('printPiece')->with(['board' => $board, 'piece' => $piece]);
    }

//    public function mytest(Request $request)
//    {
//        $boardPiecesObj = new BoardPiece;
//        $movesObj = new Move;
//        $user = Auth::user();
//        $game = Game::where('user_id', $user->id)->where('status', true)->first();
//        if (empty($game)) {
//            return redirect('/board');
//        }
//        $board = $game->board()->first();
//        if ($request->has('nextGame')) {
//            Game::where('id', $game->id)->update(['status' => false]);
//            BoardPiece::where('is_active', true)->where('board_id', $board['id'])->update(['is_active' => false]);
//            $game = Game::where('user_id', $user->id)->where('status', true)->first();
//            if (empty($game)) {
//                return redirect('/board');
//            }
//        }
//        echo "Game " . $game['id'] . "<br>";
//        echo "Board " . $board['id'];
//        if ($request->has('startGame')) {
//            session()->put('gameOver', false);
//            $boardPieceGameObj = new BoardPieceGame($board['rows'], $board['columns']);
//            $boardPieces = BoardPiece::where('is_active', true)->where('board_id', $board['id'])->get();
//            foreach ($boardPieces as $boardPiece) {
////                if($boardPiece['x']>$board['rows'] || $boardPiece['y']>$board['columns'])
////                {
////                    BoardPiece::where('piece_id',$boardPiece['piece_id'])->where('board_id',$board['id'])->update(["commands"=>"","is_active"=>false]);
////                }
//                $directions = explode(",", $boardPiece['commands']);
//                if (!empty($directions)) {
//                    $direction = array_shift($directions);
//                    if ($direction != null) {
//                        $newDirections = implode(',', $directions);
//                        $movesObj->createMove($direction, $board['id'], $boardPiece['piece_id']);
//                        $boardPiecesObj->updateBoardPiece($boardPiece['piece_id'], $newDirections);
//                    }
//                } else {
//                    break;
//                }
//            }
//
//            //   $board=$game->board()->first();
//
////            $moveDatas=Move::where('board_id',$board['id'])->where('is_active',true)->first();
////            $moveId=$moveDatas['id'];
////            if($moveDatas!=null)
////            {
////                $getPieceData=BoardPiece::where('is_active',true)->where('piece_id',$moveDatas['piece_id'])->first();
////                $boardPieceGameObj->createPiece($getPieceData['x'], $getPieceData['y']);
////                $newPiece=$boardPieceGameObj->move($moveDatas['command'],$boardPieceGameObj->piece);
////                if(session()->get('flag') && !$boardPiecesObj->isBoardPieceExists($newPiece,$moveDatas['pieceId']))
////                {
////                    $boardPiecesObj->updatePiecePosition($moveDatas['piece_id'],$newPiece);
////                }
////                else
////                {
////
////                    Move::where('piece_id',$moveDatas['piece_id'])->where('is_active',true)->update(["command"=>"","is_active"=>false]);
////                    BoardPiece::where('piece_id',$moveDatas['piece_id'])->where('is_active',true)->update(["commands"=>""]);
////
//////                        foreach ($moves as $move)
//////                        {
//////                            $move->command="";
//////                            $move->is_active=false;
//////                            $move->save();
//////                        }
//////                        Move::where('piece_id', $moveDatas['piece_id'])
//////                            ->where('is_active', true)
//////                            ->update(['command' => "",'is_active'=>false]);
////                    // Move::where('piece_id',$moveDatas['piece_id'])->where('is_active',true)->update(['command'=>"",'is_active'=>false]);
////                }
////                $movesObj->updateMove($moveId);
////                session()->put('gameOver',false);
////            }
////            $boardPieceGameObj=new BoardPieceGame($board['rows'],$board['columns']);
////            session()->put('flag',true);
////            $boardPieces = BoardPiece::where('is_active', true)->where('board_id',$board['id'])->get();
////            foreach ($boardPieces as $boardPiece) {
//////                if($boardPiece['x']>$board['rows'] || $boardPiece['y']>$board['columns'])
//////                {
//////                    BoardPiece::where('piece_id',$boardPiece['piece_id'])->where('board_id',$board['id'])->update(["commands"=>"","is_active"=>false]);
//////                }
////                $directions = explode(",", $boardPiece['commands']);
////                if (!empty($directions)) {
////                    $direction = array_shift($directions);
////                    if ($direction != null) {
////                        $newDirections = implode(',', $directions);
////                        $movesObj->createMove($direction,$board['id'],$boardPiece['piece_id']);
////                        $boardPiecesObj->updateBoardPiece($boardPiece['piece_id'],$newDirections);
////                    }
////                } else {
////                    break;
////                }
////            }
//
//        }
//        $game = Game::where('user_id', $user->id)->where('status', true)->first();
//        $board = $game->board()->first();
//        $piece = [];
//        $commands = [];
//        $boardPieces = BoardPiece::where('is_active', true)->where('board_id', $board['id'])->get();
//        foreach ($boardPieces as $boardPiece) {
//            $piece[] = ['x' => $boardPiece['x'], 'y' => $boardPiece['y']];
//            $commands[] = $boardPiece['commands'];
//        }
//        session()->put('flag', true);
//        session()->put('gameOver', false);
//        return view('printPiece')->with(['board' => $board, 'piece' => $piece, 'commands' => $commands]);
//    }
//
////    public function isPieceExists($piece)
////    {
////        $pieces=Piece::get();
////        foreach ($pieces as $pieceData)
////        {
////            if($pieceData['x']==$piece['x'] && $pieceData['y']==$piece['y'])
////            {
////                return true;
////            }
////        }
////        return false;
////    }
////    public function randomPiece()
////    {
////        $count=1;
////        $board=Board ::orderBy('id', 'desc')->first();
////        while ($count!=0)
////        {
////            $piece['x']=rand(1,$board['rows']);
////            $piece['y']=rand(1,$board['columns']);
////            if($this->isPieceExists($piece))
////            {
////                $count++;
////            }
////            else
////            {
////                $count=0;
////            }
////        }
////        return $piece;
////
////    }
//
//    public function testing()
//    {
//        $board['rows']=4;
//        $board['columns']=4;
//        $randomPiece=3;
//        $boardPieceObj=new BoardPiece;
//        $pieceObj=new Piece;
//        for($i=0;$i<$randomPiece;$i++)
//        {
//            $flag=true;
//            $piece['x']=rand(1,$board['rows']);
//            $piece['y']=rand(1,$board['columns']);
//            if($boardPieceObj->isPieceExists($piece,$this->board['id']))
//            {
//                $flag=false;
//                break;
//            }
//            if($flag)
//            {
//                if(!$pieceObj->isPieceExists($piece))
//                {
//                    $pieceObj->createPiece($piece);
//                }
//                $getPiece=$pieceObj->getPieceId($piece);
//                $boardPieceObj->createBoardPiece($board['id'],$piece);
//
//            }
//            else
//            {
//                $i=$i-1;
//            }
//        }
//    }

}