<?php
/**
 * Created by PhpStorm.
 * User: levaral
 * Date: 21/02/18
 * Time: 6:07 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Game;
use App\Move;
use App\Board;
use App\BoardPiece;
use App\Piece;
use App\Games\BoardPieceGame;

class BoardPieceGameController extends Controller
{
    public function newGame(Request $request)
    {
        $user=Auth::user();
        $game=$user->game()->where('status',true)->first();
        if (!empty($game)) {
            return redirect('displaygame');
        }
        $piecesObj=new Piece;
        $boardsObj=new Board;
        $gamesObj=new Game;
        $boardPiecesObj=new BoardPiece;
        $piece=[];
        if($request->has('newGame')) {
            $this->validate($request, [
                'rows' => 'required|integer',
                'columns' => 'required|integer'
            ]);
            $rows = $request->input('rows');
            $columns = $request->input('columns');
            $counterOfPiece = $request->input('counter');
            $board['rows'] = $rows;
            $board['columns'] = $columns;
            $gamesObj->createGame($user->id);
            $game_Id = $gamesObj->getGameId($user->id);
            $boardsObj->createBoard($board, $game_Id);
            $game=$user->game()->where('status',true)->first();
            $board = $game->board()->first();
            for ($count = 1; $count <= $counterOfPiece; $count++) {
                $this->validate($request, [
                    'x' . $count => 'required|integer',
                    'y' . $count => 'required|integer',
                    'commands' . $count => 'required|string',
                ]);
                $piece['x'] = $request->input('x' . $count);
                $piece['y'] = $request->input('y' . $count);
                $commandSel = $request->input('commands' . $count);
                $length=strlen($commandSel);
                if($commandSel[$length-1]==",")
                 {
                     $commandSel=substr($commandSel,0,-1);
                 }
                $commands=$commandSel;
                if ($piece['x'] <= $board['rows'] && $piece['y'] <= $board['columns']) {
                    if (!$boardPiecesObj->isPieceExists($piece, $board['id'])) {
                        if (!$piecesObj->isPieceExists($piece)) {
                            $piecesObj->createPiece($piece);
                        }
                        $getPiece = $piecesObj->getPiece($piece);
                        $boardPiecesObj->createBoardPiece($board['id'], $getPiece, $commands);
                    }
                }
            }
            session()->put('flag', true);
            session()->put('gameOver', false);
            return redirect('displaygame');

        }
        return view('getBoardData');
    }
    public function startGame(Request $request)
    {
        $boardPiecesObj = new BoardPiece;
        $movesObj = new Move;
        $user=Auth::user();
        $game=$user->game()->where('status',true)->first();
        if(empty($game))
        {
            return redirect('newgame');
        }
        $board = $game->board()->first();
        if($request->has('nextGame'))
        {
            Game::where('id',$game->id)->update(['status'=>false]);
            $game=$user->game()->where('status',true)->first();
            if (empty($game)) {
                return redirect('newgame');
            }
        }
        if ($request->has('startGame')) {
            session()->put('gameOver', false);
            $boardPieceGameObj = new BoardPieceGame($board['rows'], $board['columns']);
            session()->put('flag', true);
            $boardPieces=$board->board_piece()->get();
            foreach ($boardPieces as $boardPiece) {
                $directions = explode(",", $boardPiece['commands']);
                if (!empty($directions)) {
                    $direction = array_shift($directions);
                    if ($direction != null) {
                        $newDirections = implode(',', $directions);
                        $movesObj->createMove($direction, $board['id'], $boardPiece['piece_id']);
                        $boardPiecesObj->updateBoardPiece($boardPiece['piece_id'],$boardPiece['board_id'],$newDirections);
                    }
                } else {
                    break;
                }
            }
            $move=$board->move()->where('is_active',true)->first();
            if ($move != null) {
                $getBoardPiece = BoardPiece::where('board_id',$move['board_id'])
                    ->where('piece_id', $move['piece_id'])->first();
                $boardPieceGameObj->createPiece($getBoardPiece['x'], $getBoardPiece['y']);
                $piece = $boardPieceGameObj->move($move['command'], $boardPieceGameObj->piece);
                if (session()->get('flag') && !$boardPiecesObj->isBoardPieceExists($piece,$move['board_id'], $move['pieceId'])) {
                    $boardPiecesObj->updatePiecePosition($move['piece_id'],$move['board_id'], $piece);
                } else {
                    Move::where('piece_id', $move['piece_id'])
                        ->where('board_id',$move['board_id'])
                        ->where('is_active', true)
                        ->update(["command" => "", "is_active" => false]);
                    BoardPiece::where('piece_id', $move['piece_id'])
                        ->where('board_id',$move['board_id'])
                        ->update(["commands" => ""]);
                }
                $movesObj->updateMove($move['id']);
                session()->put('gameOver', false);
            }
            else {
                session()->put('gameOver', true);
                Game::where('id', $game->id)->update(['status' => false]);
            }
            if (!session()->get('gameOver')) {
                ?>
                <script>
                    setTimeout(function () {
                        location.reload()
                    }, 2000);
                </script>
                <?php
            }
            $piece = [];
            $boardPieces=$board->board_piece()->get();
            foreach ($boardPieces as $boardPiece) {
                $piece[] = ['x' => $boardPiece['x'], 'y' => $boardPiece['y']];
            }
            $gameCounter=$user->game()->where('status',true)->count();
            return view('printPiece')->with(['board' => $board, 'piece' => $piece,'gameCounter'=>$gameCounter]);
        }
        $game=$user->game()->where('status',true)->first();
        $board = $game->board()->first();
        $piece = [];
        $boardPieces=$board->board_piece()->get();
        foreach ($boardPieces as $boardPiece) {
            $piece[] = ['x' => $boardPiece['x'], 'y' => $boardPiece['y']];
        }
        session()->put('flag', true);
        session()->put('gameOver', false);
        $gameCounter=$user->game()->where('status',true)->count();
        return view('printPiece')->with(['board' => $board, 'piece' => $piece,'gameCounter'=>$gameCounter]);
    }

}