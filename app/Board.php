<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $table="boards";
    protected $fillable=["id","rows","columns","game_id"];
    public function game()
    {
        return $this->belongsTo('App\Game');
    }
    public function board_Piece()
    {
        return $this->hasMany('App\BoardPiece');
    }
    public function move()
    {
        return $this->hasMany('App\Move');
    }
    public function createBoard($board,$game_id)
    {
        Board::create(['rows'=>$board['rows'],'columns'=>$board['columns'],'game_id'=>$game_id]);
    }
//    public function isBoardExists($board)
//    {
//        $boardDatas=Board::all();
//        foreach ($boardDatas as $boardData)
//        {
//            if($boardData['rows']==$board['rows'] && $boardData['columns']==$board['columns'])
//            {
//                return true;
//            }
//        }
//        return false;
//    }
//    public function updateBoard($board,$game_id)
//    {
//        Board::where('rows', $board['rows'])
//            ->where('columns',$board['columns'])
//            ->update(['game_id' => $game_id]);
//
//    }
}
