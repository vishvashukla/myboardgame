<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    protected $table="pieces";
    protected $fillable=["id","x","y"];
    public function boardPiece()
    {
        return $this->hasMany('App\BoardPiece');
    }
    public function moves()
    {
        return $this->hasMany('App\Moves');
    }
    public function getPiece($piece)
    {
        $pieces=Piece::where('x',$piece['x'])
            ->where('y',$piece['y'])->get();
        foreach ($pieces as $pieceData)
        {
            $piece['id']=$pieceData['id'];
            $piece['x']=$pieceData['x'];
            $piece['y']=$pieceData['y'];
        }
        return $piece;
    }

    public function createPiece($piece)
    {
        Piece::create(['x'=>$piece['x'],'y'=>$piece['y']]);
    }
    public function isPieceExists($piece)
    {
        $pieces=Piece::get();
        foreach ($pieces as $pieceData)
        {
            if($pieceData['x']==$piece['x'] && $pieceData['y']==$piece['y'])
            {
                "match";
                return true;
            }
        }
        return false;
    }
    public function randomPiece()
    {
        $count=1;
        $board=Board ::orderBy('id', 'desc')->first();
        while ($count!=0)
        {
            $piece['x']=rand(1,$board['rows']);
            $piece['y']=rand(1,$board['columns']);
            if($this->isPieceExists($piece))
            {
                $count++;
            }
            else
            {
                $count=0;
            }
        }
        return $piece;
    }
}
