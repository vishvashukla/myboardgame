@extends('layouts.app')
@section('content')
    <html>
    <head>
        <link rel="icon" type="image/gif/png" href="{{asset('images/piece.png')}}">
        <title>Board</title>
        <link rel="stylesheet" href="css/styles.css">

    </head>
    <body>
    <form method="post" action="{{route('displaygame')}}">
        {{ csrf_field() }}
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <input type="submit" name="startGame" value="Start Game" class='button' style="margin-left: 200px">
                            <input type="submit" name="nextGame" value="Next Game" class='button' style="margin-left: 10px">
                            <br>
        Active Game {{$gameCounter}}
        <table border='solid' name='myTable' id='myTable' cellpadding='2px' align='center' style="margin-top: 20px">
            @if(session()->has('validPiece'))
                <h5 align="center">Your Piece Position is out of range of Board</h5>
                {{session()->forget('validPiece')}}
            @endif
            {{--    @for($countPiece=1;$countPiece<=sizeof($piece);$countPiece++)--}}
            @for($row = $board['rows']; $row >= 1; $row--)
                <tr>
                    @for($column = 1; $column <= $board['columns']; $column++)
                        <td style="height: 100px;width: 100px; " align="center">

                            @for($countPiece=0;$countPiece<sizeof($piece);$countPiece++)
                                @if($row == $piece[$countPiece]['x'] && $column == $piece[$countPiece]['y'])
                                    <img src="{{asset('images/piece.png')}}" style="margin-top: 10px" height='20px' width='20px'/><br>p{{$countPiece+1}}
                                @endif
                            @endfor
                        </td>

                    @endfor
                </tr>
            @endfor
            {{--  @endfor--}}
        </table>

        @if(!session()->get('flag'))

            <h2 style="color: purple;" align="center">Invalid Move</h2>
        @endif
        @if(session()->get('gameOver'))

            <h2 style="color: red;" align="center">Game Over</h2>
        @endif
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </body>
    </html>

@endsection
