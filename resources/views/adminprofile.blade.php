@extends('layouts.app')
@section('content')
<html>
<head>
    <title>Admin</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if(session()->has('user'))
                        <h1>You don't have privilege to view that page</h1>
                    @endif
                    <h1 align="center">Hello Admin</h1>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
    @endsection