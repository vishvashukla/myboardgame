@extends('layouts.app')
@section('content')

    <html>
    <head>

        <link rel="icon" type="image/gif/png" href="{{asset('images/piece.png')}}">
        <title>My Board</title>
        <link rel="stylesheet" href="css/styles.css">
        <script>
            function validateForm() {
                var rows = document.forms["myForm"]["rows"].value;
                var columns = document.forms["myForm"]["columns"].value;
                if (rows == "") {
                    alert("Enter Rows");
                    return false;
                }
                if (columns == "") {
                    alert("Enter columns");
                    return false;
                }

            }
        </script>
    </head>
    <body>
    <form method="post" action="{{route('newgame')}}" name="myForm" onsubmit="return validateForm()">
        {{ csrf_field() }}
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table align="center">
                                <tr>
                                    <td>Enter Rows</td>
                                    <td><input type="number" name="rows" required="true"></td>
                                </tr>
                                <tr>
                                    <td>Enter Columns</td>
                                    <td><input type="number" name="columns" required="true"></td>
                                </tr>
                                <tr><td></td><td></td></tr>
                                <tr>
                                    <td colspan="2"><input type="button" name="addPiece" value="Add Pieces" onclick="addPieces()" class='button' style="margin-top: 5px;"></td>
                                </tr>
                            </table>

                            <span id="response"></span>
                            <script>
                                var count=1;
                                var boxName=0;
                                function addPieces() {
                                    var xCounter="x"+count;
                                    var yCounter="y"+count;
                                    var commandsCounter="commands"+count;
                                    var Counter=count;
                                    var CounterName="counter";
                                    document.getElementById('response').innerHTML+='<br><table align="center"><tr align="center"><td>'
                                        +'<tr align="center"><td>x:</td><td><input type="number" required="true" id="'+boxName+'"name="'+xCounter+'""/></td></tr>'
                                        +'<tr align="center"><td>y:</td><td><input type="number" required="true" id="'+boxName+'"name="'+yCounter+'""/></td></tr>'
                                        +'<tr align="center"><td>Commands:</td><td><input type="text" required="true" id="'+commandsCounter+'"name="'+commandsCounter+'"" /></td>' +
                                        '<td><select id="'+count+'" style="margin-left: 10px" onchange="addCommand(this.id)">'+
                                        '<option value="0">Select Command</option>'+
                                        '<option value="1">left</option>'+
                                        '<option value="2">right</option>'+
                                        '<option value="3">up</option>'+
                                        '<option value="4">down</option>]'+
                                        '<option value="5">other</option>]'+
                                        '<option value="6">clear</option>]'+
                                        '</select></td>'+
                                        '</tr></table>'
                                        +'<input type="hidden" value="'+Counter+'"name="'+CounterName+'""/>';

                                    count+=1;
                                }

                                function addCommand(selectId) {
                                    var commandsCounter="commands"+selectId;
                                    var sel = document.getElementById(selectId);
                                    /*var value = sel.options[sel.selectedIndex].value; // or sel.value*/
                                    var text = sel.options[sel.selectedIndex].text;
                                    if(text=="clear")
                                    {
                                        var myText=document.getElementById(commandsCounter);
                                        myText.value="";
                                    }
                                    else
                                    {
                                        var myText=document.getElementById(commandsCounter);
                                        myText.value+=text+',';
                                    }
                                    document.getElementById(selectId).selectedIndex = 0;
                                }
                            </script>
                            <input  type="submit" name="newGame" value="New Game" class='button' style='margin-left: 300px;margin-top: 5px;'>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </body>
    </html>

@endsection