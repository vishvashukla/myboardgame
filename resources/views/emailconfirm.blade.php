@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class=”panel-heading”><h1 align="center">Registration Confirmed</h1></div>
                <div class=”panel-body”>
                   <h2 align="center"> Your Email is successfully verified. Please Login</h2>
                </div>
                </div>
            </div>

            </div>
        </div>

@endsection