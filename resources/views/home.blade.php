@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                {{--<div class="panel-heading">Dashboard</div>--}}

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(session()->has('admin'))
                        <h1>You don't have privilege to view that page</h1>
                        @endif
                   <h2 align="center"> <a href="{{route('newgame')}}">Board Game</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
